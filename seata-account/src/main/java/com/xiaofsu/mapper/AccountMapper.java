package com.xiaofsu.mapper;

import com.xiaofsu.pojo.Account;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AccountMapper {

    @Update("update tb_account set a_count = a_count - #{account.count} where id = #{account.id}")
    void updateAccount(@Param("account") Account account);

}
