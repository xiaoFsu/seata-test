package com.xiaofsu.controller;

import com.xiaofsu.mapper.AccountMapper;
import com.xiaofsu.pojo.Account;
import com.xiaofsu.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@RestController
public class AccountController {

    @Autowired
    private AccountMapper accountMapper;

    @PostMapping("/updateAccount")
    public boolean updateAccount(@RequestBody Account account, HttpServletRequest request){
        Enumeration<String> headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()){
            System.out.println(headerNames.nextElement());
        }
        System.out.println(request.getHeader("tx_xid"));
        accountMapper.updateAccount(account);
        return true;
    }

}
