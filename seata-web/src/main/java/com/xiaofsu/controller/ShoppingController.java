package com.xiaofsu.controller;

import cn.hutool.json.JSONUtil;
import com.alibaba.nacos.common.utils.UuidUtils;
import com.xiaofsu.pojo.Account;
import com.xiaofsu.pojo.Order;
import com.xiaofsu.service.AccountService;
import com.xiaofsu.service.OrderService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShoppingController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private AccountService accountService;

    @RequestMapping("/shopping")
    @GlobalTransactional(rollbackFor = Exception.class)
    public boolean shopping(){
        String xid = RootContext.getXID();



        Order order = new Order(UuidUtils.generateUuid(),"上海市",null);
        Account account = new Account("1","23");

        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println(xid);
        System.out.println(JSONUtil.toJsonStr(order));
        System.out.println(JSONUtil.toJsonStr(account));

        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        boolean b = accountService.updateAccount(account);
        System.out.println("account transactional : "+ b);
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        boolean b1 = orderService.addOrder(order);
        System.out.println("order transactional : "+ b1);
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
        return true;
    }

}
