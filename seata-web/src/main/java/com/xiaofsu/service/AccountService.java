package com.xiaofsu.service;

import com.xiaofsu.pojo.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("seata-account")
public interface AccountService {

    @PostMapping("/updateAccount")
    boolean updateAccount(@RequestBody Account account);
}
