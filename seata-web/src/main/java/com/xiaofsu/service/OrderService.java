package com.xiaofsu.service;

import com.xiaofsu.pojo.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("seata-order")
public interface OrderService {

    @PostMapping("/addOrder")
    boolean addOrder(@RequestBody Order order);

}
