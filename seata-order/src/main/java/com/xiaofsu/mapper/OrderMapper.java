package com.xiaofsu.mapper;

import com.xiaofsu.pojo.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderMapper {

    @Insert("insert into tb_order(id,address) values (#{order.id},#{order.address})")
    void addOrder(@Param("order")Order order);

}
