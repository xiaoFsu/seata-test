package com.xiaofsu.controller;

import com.xiaofsu.mapper.OrderMapper;
import com.xiaofsu.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@RestController
public class OrderController {

    @Autowired
    private OrderMapper orderMapper;

    @PostMapping("/addOrder")
    public boolean addOrder(@RequestBody Order order, HttpServletRequest request) throws Exception{
        Enumeration<String> headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()){
            System.out.println(headerNames.nextElement());
        }
        System.out.println(request.getHeader("tx_xid"));
        if(order.getCreateTime() == null){
            throw new Exception("错误");
        }
        orderMapper.addOrder(order);
        return true;
    }

}
