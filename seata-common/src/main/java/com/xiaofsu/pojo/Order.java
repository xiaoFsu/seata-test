package com.xiaofsu.pojo;

public class Order {

    private String id;
    private String address;
    private String createTime;

    public Order() {
    }

    public Order(String id, String address, String createTime) {
        this.id = id;
        this.address = address;
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
